from math import sin, pi, sqrt, cos


def seno(alfa,n = 100):
    b = alfa/(2**n)
    for i in range(n):
        b = 2*b*(sqrt(1-(b**2)))
    return(b)

lista=[0.01,0.1,0.5,1,1.5,pi]

for i in lista:
    print(i,seno(i))
    print(i,sin(i))
    print()

print("-------------------\n")

def coseno(alfa):
    cose = seno((pi/2)-alfa)
    return(cose)

for i in lista:
    print(i,coseno(i))
    print(i,cos(i))
    print()


