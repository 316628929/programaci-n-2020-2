# Calcular raíces de: x^2 + 3x - 5 = 10

from math import sqrt

a = 1
b = 3
c = -15

raiz1 = (-b + sqrt((b**2)-(4*a*c)))/2
raiz2 = (-b - sqrt((b**2)-(4*a*c)))/2
print("Las raices de x^2 + 3x - 5 = 10 son %f y %f." %(raiz1, raiz2))


###También hice está:
##
##def esg(ecuacion):
##    """de forma: ax^2+bx+c=0, escribiendo todos los coeficeientes, como cadena"""
##
##    from math import sqrt
##
##    ec = ecuacion
##
##    ec = ec.replace('=0','')
##    ec = ec.replace('x+',' ')
##    ec = ec.replace('x^2+',' ')
##
##    ec = ec.split(' ')
##
##    ec = list(map(int,ec))
##
##    a = ec[0]
##    b = ec[1]
##    c = ec[2]
##
##    raiz1 = (-b + sqrt((b**2)-(4*a*c)))/2
##    raiz2 = (-b - sqrt((b**2)-(4*a*c)))/2
##
##    return(raiz1,raiz2)


# --------------------------------------------
# Calcular el área de un círculo de radio 10.5

from math import pi

r = 10.5
A = pi * (r**2)

print("El área de un cícrulo de radio 10.5 es %f."%(A))

# ----------------------------------
# Calcular 10 factorial paso a paso.
# Ño

f = 1

for i in range(1,10):
    f *= i

print("El factorial de 10 es %i" %(f))

# --------------------
# No encontré el libro

# ----------------------------------------------
# Convertir de metros a pies, de kilos a libras,
# de pesos a dólares, de Kilowatts a Amperes en
# un circuito de 120 volts, de Fahrenheit a 
# Kelvin y viceversa.

def pie(m):
    p = m * 3.28084
    print(p)

def libra(k):
    l = k * 2.20462
    print(l)

def dolar(p):
    d = p * 0.054
    print(d)

def amper(k):
    a = (k * 1000)/120
    print(a)

def fahrenheit(k):
    f = (k - 273.15) * 9/5 + 32
    print(f)

def kelvin(f):
    k = (f - 32) * 5/9 + 273.15
    print(k)

