# El car  ́a c t e r alm o h a d ill a , s i g n o de n ́umero , o hash i n d i c a e l i n i c i o de uncomen tar io . Todo l o que se e s c r i b a despu  ́e s d e l car  ́a c t e r de comen tar io py thonno l o i n t e r p r e t a . Los c omen t a r i o s s i r v e n para d e s c r i b i r l o que hace l  ́ı nea enl a que se e s c r i b e .
3 + 5 # l a suma de dos e n t e r o s
3 - 2 # l a r e s t a de dos e n t e r o s
6 * 7 # e l p r o d uc t o de dos e n t e r o s
8/4 # l a d i v i s i  ́on de dos e n t e r o s
9/4 # o t r a d i v i s i  ́on de e n t e r o s , ¿El r e s u l t a d o e s e n t e r o ?
3 + 2 - 5 + 1 # o p e r a c i  ́on ar i tm  ́e t i c a de c u a t r o e n t e r o s
5**2 # ¿ e l d o b l e a s t e r i s c o qu  ́e o p e r a c i  ́on r e a l i z a ?
( 3 + 2 ) *( 5 - 4 ) # ¿ e l r e s u l t a d o e s 21?
-(5*1) #
9 %5 # ¿Cu ́a l e s e s t a o p e r a c i  ́on?
8 / 2*( 2 + 2 ) # Primero r e s u e l v e manualmente l a e x p r e s i  ́on . ¿El r e s u l t a d o manual ye l de py thon son e l mismo?
a = 9
b = a ; h = 1
b = ( b+h ) / 2; h=a /b
type( a )
type( b )
type( h )
c = " e s t o e s una cadena 2"
d = " e s t o e s o t r a cadena "
print( c )
print( d )
e = """Una cadena
m u l t i l i n e a .
Tres l i n e a s
Cuatro l i n e a s
"""
f = "The Zen o f Python "
g = """ B e a u t i f u l i s b e t t e r than u g l y .
E x p l i c i t i s b e t t e r than i m p l i c i t .
S imple i s b e t t e r than complex .
Complex i s b e t t e r than c om pl i c a t e d .
F l a t i s b e t t e r than n e s t e d .
S p ar se i s b e t t e r than dense .
R e a d a b i l i t y c o u n t s .
S p e c i a l c a s e s aren ’ t s p e c i a l enough t o b r e ak t h e r u l e s .
Al t h oug h p r a c t i c a l i t y b e a t s p u r i t y .
Err or s s h o ul d neve r p a s s s i l e n t l y .
Unle s s e x p l i c i t l y s i l e n c e d .
In t h e f a c e o f amb ig u i ty , r e f u s e t h e t em p t a t i o n t o g u e s s .
There s h o ul d be one-- and p r e f e r a b l y o nly one --o b v i o u s way t o do i t .
Al t h oug h t h a t way may no t be o b v i o u s a t f i r s t u n l e s s you ’ re Dutch .
Now i s b e t t e r than neve r .
Al t h oug h neve r i s o f t e n b e t t e r than * r i g h t * now .
I f t h e im plemen t a t i on i s hard t o e x pl a i n , i t ’ s a bad i d e a .
I f t h e im plemen t a t i on i s e a sy t o e x pl a i n , i t may be a good i d e a .
Namespaces are one h onk ing g r e a t i d e a -- l e t ’ s do more o f t h o s e !
"""

12

print( e )
print( f , "\n" , g )
