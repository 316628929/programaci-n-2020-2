#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 19:02:17 2020

@author: dario
"""

def limpiaPantalla():
    """
    Limpia la pantalla de la terminal.
    Dependiendo del sistema operativo en que se ejecute la función
    se ejecutará el comando correspondiente para borrar la pantalla.
    """
    import platform, os
    if platform.system() == 'Linux' :
        os.system('clear')
    elif platform.system() == 'Windows' :
        os.system('cls')
    elif platform.system() == 'Darwin' :
        os.system('clear')
        
def espera():
    return(input("Presiona enter para continuar"))

def agregaRutas(rutas,home=""):
    from sys import path
    if home == "home":
        from pathlib import Path
        home = str(Path.home())
    for ruta in rutas:
        path.insert(0,home + ruta)
        
