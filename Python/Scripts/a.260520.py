
ls1 = [5,10,15,20,25]
res = []
for i in ls1:
    res.append(i**2)
print(res)

res = [i**2 for i in ls1]
print(res)

ls2 = [1,2,3,4,5,6,7]
res = []
for i in ls2:
    if i%2==0:
        res.append(i**2)
print(res)

res = [i**2 for i in ls2 if i%2==0]
print(res)

res = []
for i in ls2:
    if i%2==0:
        res.append(i**2)
    else:
        res.append(i**3)
print(res)

res = [i**2 if i%2==0 else i**3 for i in ls2]
print(res)

ls1 = [9,3,6,1,5,0,8,2,4,7]
ls2 = [6,4,6,1,2,2]

l = []
for i in ls2:
    l.append((i,ls1.index(i)))
print(l)

l = [(i,ls1.index(i)) for i in ls2]
print(l)

d = {i:ls1.index(i) for i in ls2}
print(d)

