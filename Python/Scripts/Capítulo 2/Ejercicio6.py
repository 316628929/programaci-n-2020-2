# Darío
# Ejercicio 6, Cpítulo 2

""" Los identificadores , o variables pueden cambiar su valor
son aquellos que definimos, requieren ser nombreados por el usuario.
un ejemplo sería al escribir en la consola:
a = 4
"a" es un identificador, o una variable.
Una literal es un número, como 1, 2 o 3, estos no cambian su valor,
no requieren ser nombrados o definidos por el usuario y no se identifican
de alguna otra manera más que su representación "literal"
"""
