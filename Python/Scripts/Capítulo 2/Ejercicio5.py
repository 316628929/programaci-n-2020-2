# Darío
# Ejercicio 5, Capítulo 2

""" El libro dice:

In summary, in addition to a value, a Python object also has
an identify and a type. Once constructed, the identify and
type of an object cannot be changed. For number objects, the
value also cannot be changed after the object is constructed.

Esto nos dice que el valor es la propiedad que se puede
modificar después de la construcción del objeto, mientras que
el tipo y el identificador no.
