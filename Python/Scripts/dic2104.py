

dic = {'Nombre': 'Fulanito', 'Edad':15, 'Materias':['Matemáticas','Física','Historia']}

print(dic)

print(dic['Nombre'])
print(dic['Edad'])
print(dic['Materias'])
print(dic['Materias'][2])

dic2 = {"Nombre":"Perenganito",11:'Edad'}

for i in dic2:
    print(i)
    print(dic2[i])


# --------------------------------------------------------------------
# Escribe un programa que pregunte al usuario su nombre, edad, direccion y
# telefono y lo guarde en un diccionario. Despues hay que mostrar por pantalla
# el mensaje <nombre> tiene <edad> años, vive en <direccion> y su numero de
# telefono es <telefono>


Datos = {'Nombre':input("Introducir nombre"),'Edad':input("Introducir edad"),'Dirección':input("Introducir dirección"),'Teléfono':input("Introducir télefono")}

print("%s tiene %s años, vive en %s y su teléfono es %s" %(Datos['Nombre'],Datos['Edad'],Datos['Dirección'],Datos['Teléfono']))

# --------------------------------------------------------------------
#Escribe un programa que guarde en un diccionario los precios de las frutas de
#la tabla, pregunta al usuario por una fruta y una cantidad en kilos, muestra
#despues en pantalla el precio de ese numero de kilos de fruta. Si la fruta
#No se encuentra en el diccionario debes mostrar un mensaje "No se encuentra esa fruta en el inventario"
#Platano 10 pesos
#Mazana 15 pesos
#Pera 20 pesos
#Naranja 5 pesos

Frutas = {'Platano':10,'Manzana':15,'Pera':20,'Naranja':5}

f = input("Introduzca la fruta")
p = 0

for i in Frutas:
    if i == f:
        p = Frutas[i]
        k = int(input("Introducir kilos"))
        s = p*k

        print("El precio es de %i pesos" %(s))

if p == 0:
    print("No se encuenra la fruta")


