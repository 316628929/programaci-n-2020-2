

print("Valores Booleanos")
cadena =  "Una cadena"
x = 5.1
n = 15
l = [cadena, x, n]
cadenaVacia = ""
y = 0.0
m = 0
lVacia = []

print(bool(cadena))
print(bool(x))
print(bool(n))
print(bool(l))
print(bool(cadenaVacia))
print(bool(y))
print(bool(m))
print(bool(lVacia))

print("Operadores lógicos o booleanos")
x = 0
y = 10

print(x or y)
print(x and y)
print(not y)


#--------------------------------

ln=list(range(0,101))

while bool(ln):
	print(ln.pop())
