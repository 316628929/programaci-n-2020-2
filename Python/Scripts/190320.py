# Darío

## for variable in elemento iterable(lista, cadena, range, etc)
##        cuerpo del ciclo for
##
## Primer program utilizando for

print("Comenzamos")

for i in [0,1,2]:
      print("Hola", end="")
print()
print("Final")

list = [1,3,5,7,9]
for i in list:
    print(i)

# ----------------------------------------------------------
## Escribir un programa que pida al usuario
## una palabra y la muestre 15 veces en la pantalla.

a = input("Introduce la palabra a imprimir")

for i in range(0,14):
    print(a)

# ----------------------------------------------------------
## Escribir un programa que pregunte al usuario su edad
## y muestre por pantalla todos los años que ha cumplido
## desde que tenía 1 año de edad.

e = int(input("Introduzca edad"))

## Comencé desde 2, pues ya que tienes 1 año, no cumples 1 año
for i in range(2,e+1):
    print(i)

## Este empieza en 1, para variarle
for i in range(e):
    print("Hasta hoy has cumplido " + str(i+1) + " años")
    
## ---------------------------------------------------------
## Escribir un programa que almacene las asignaturas de un curso
## y que en la pantalla se muetren.

## Solo imprime la lista, pues solo es necesario mostrar asignaturas
## Está comentada la forma de hacerlo con un ciclo for
lista = ["Programación", "Cálculo", "Álgebra"]
print(lista)
##for i in lista:
##    print(i)

# ----------------------------------------------------------
