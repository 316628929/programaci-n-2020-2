#print("Solución de cinco formas distintas de -1 + 2x^2 + 3x^7")

def p(x):
    return(-1 + x**2 +3*(x**7))

#print(p(5))

# ----------------------------

lista = [-1,0,1,0,0,0,0,3]

def pol(x, lista):
    suma = 0
    cont = 0
    for i in lista:
        suma += i*(x**(cont))
        cont += 1
    return(suma)

#print(pol(5,lista))

# ----------------------------

dic = {0:-1,2:1,7:3}

def poli(x, dic):
    suma = 0
    for i in dic:
        suma += (x**i)*dic[i]
    return(suma)

#print(poli(5,dic))

# ----------------------------

lista1 = [[0,-1],[2,1],[7,3]]

def polin(x, lista):
    suma = 0
    for i in lista:
        suma += (x**i[0])*i[1]
    return(suma)
    
#print(polin(5,lista1))
        
# ----------------------------------------------------------

def polino(x,lista):
    return(sum([lista[i]*x**i for i in range(len(lista))]))

#print(polino(5,lista))

# ----------------------------------------------------------

#print()
#print("Solución de seis formas distintas de 1-2x¹⁰⁰")

pl = [1]+[0]*99+[2]
pd = {0:1,100:2}
pll = [[0,1],[100,2]]
plt = [(0,1),(100,2)]

#print(1*5**0 + 2*5**100)
#print(pol(5,pl))
#print(poli(5,pd))
#print(polin(5,pll))
#print(polin(5,plt))
#print(polino(5,pl))

#print()

# ----------------------------------------------------------

coeficientes = [1,5,4,8,6]

l=[]
for i in coeficientes:
    l.append(3*i)

ll = [3*i for i in coeficientes]

#print(l)
#print(ll)

#print()

# ----------------------------------------------------------

t = [27,28,26,24,28]
tf = [i*1.8 +32 for i in t]

#print(t)
#print(tf)

#print()

# ----------------------------------------------------------

def sdp(x,f,g):
    return(poli(x,f)+poli(x,g))

#print(poli(5,pd))
#print(poli(5,dic))
#print(sdp(5,pd,dic))

#print()

# ----------------------------------------------------------

def suma_polis_dic(f,g):
    dic = {}
    for i in f:
        dic[i] = f[i]
    for i in g:
        if i in f:
            dic[i] = f[i]+g[i]
        else:
            dic[i] = g[i]
    return(dic)

#print(pd)
#print(dic)
#print(suma_polis_dic(pd,dic))

#print(poli(5,suma_polis_dic(pd,dic)))

#print()

# ----------------------------------------------------------

def suma_2_polis(x,f,g):
    dic = {}
    for i in f:
        dic[i] = f[i]
    for i in g:
        if i in f:
            dic[i] = f[i]+g[i]
        else:
            dic[i] = g[i]
    suma = 0
    for i in dic:
        suma += (x**i)*dic[i]
    return(suma)

#print(suma_2_polis(5,pd,dic))

#print()

# ----------------------------------------------------------

def suma_muchos_polis(lista):
    dic={}
    for di in lista:
        for i in di:
            if i in dic:
                dic[i] = dic[i] + di[i]
            else:
                dic[i] = di[i]
    return(dic)

# ----------------------------------------------------------

def smp_sin_lista(p1,p2,*p3):
    lista = [p1,p2]+[i for i in p3]
    dic={}
    for di in lista:
        for i in di:
            if i in dic:
                dic[i] = dic[i] + di[i]
            else:
                dic[i] = di[i]
    return(dic)

# ----------------------------------------------------------

def simplificar(polinomio):
    lista = []
    for l in polinomio:
        if [l[0],0] not in lista:
            lista.append([l[0],0])
    for i in polinomio:
        for e in lista:
            if i[0] == e[0]:
                e[1] = e[1] + i[1]
    return(lista)

# ----------------------------------------------------------

def mult(f,g):
    d={}
    for x in f:
        for y in g:
            pot = x + y
            d[pot]=0
    for x in f:
        for y in g:
            pot = x + y
            coef = f[x] * g[y]
            d[pot] = d[pot] + coef
    return(d)
            
