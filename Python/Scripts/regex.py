
# Regex - secuencia de caracteres, forma patrón de búsqueda.

from re import findall

# [] Se usan para definir patrones
# eg. [a-m] ie. perimite de la 'a' a la 'm'.
# eg.
txt = 'Hola mundo'
x = findall('[a-m]',txt)
print(x)


# '.' se usa para todo menos saltos de línea.
# eg.
txt = 'hello h##o heiio he world'
x = findall('he..o',txt)
print(x)


# Secuencia especial '\d'.
# eg.
txt = 'Adrian cumplió 25 años'
x = findall('\d',txt)
print(x)


# ^ comienza con cierta secuencia.
# [^5] negación, ie. todo menos el 5.

# eg.
txt = 'Hola Mundo'
x = findall('^Hola', txt)
if x:
    print('Sí')
else:
    print('No')


#$ Para terminar cadenas.
# eg.
txt = 'Hola Mundo'
x = findall('Mundo$',txt)
if x:
    print('Sí')
else:
    print('No')


# + Más de una vez.
# eg. "a" con una o más "n".
str = 'Ana compro una manzana y una bolsa'
x = findall('an+',str)
print(x)
if x:
    print('Sí')
else:
    print('No')


# * 0 o más veces.
# eg. "a" con 0 o más "n".
str = 'Ana compro una manzana y una bolsa'
x = findall('an*',str)
print(x)
if x:
    print('Sí')
else:
    print('No')


# {} especifica las ocurrencias.
# eg. "a" seguido de una "l".
str = 'a Daniel no le gusta la comida salada'
x = findall('al{1}',str)
print(x)
if x:
    print('Sí')
else:
    print('No')

# | uno u otro.
# eg.
txt = 'Te gustan las manzanas?'
x = findall('peras|manzanas',txt)
print(x)
if x:
    print('Sí')
else:
    print('No')

