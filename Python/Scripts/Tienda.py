
caja = 1000
inv = {
    1:{'Nombre':'Coca','Precio':15,'Cantidad':1000},
    2:{'Nombre':'Sabritas','Precio':12,'Cantidad':600},
    3:{'Nombre':'Coca','Precio':10,'Cantidad':300},
    }

venta_compra = {
    'productos':{1:5,2:10,3:4},
    'total':235,
    'tipo':'compra'
    }


def ver(inv = inv):
    for i in inv:
        print('%-15s %-8i %-8s' %(inv[i]['Nombre'],inv[i]['Precio'],inv[i]['Cantidad']))

ver()
print('Hay %i en la caja' %(caja))

print()


def transaccion(formato = venta_compra,inv = inv,caja = caja):
    if formato['tipo'] == 'compra':
        n = 1
        m = -1
    else:
        n = -1
        m = 1
    total = 0
    for i in formato['productos']:
        c = formato['productos'][i]
        s = c * inv[i]['Precio']
        inv[i]['Cantidad'] += formato['productos'][i]*n
        total += s
    caja += total * m
    ver(inv)
    print('Hay %i en la caja' %(caja))

transaccion()
