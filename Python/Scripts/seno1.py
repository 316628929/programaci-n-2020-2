from math import *

alpha = 1
beta = alpha/2**5
senBeta = beta

def calculaSen2b(senBeta):    
    sen2beta = 2*senBeta*sqrt(1-(senBeta**2))
    return(sen2beta)

sen2beta = calculaSen2b(senBeta)

print("Tenemos sen(2B)=sen(alpha/2**4)="+str(sen2beta))

sen22beta = calculaSen2b(sen2beta)
print("Tenemos sen(2*2B)=sen(alpha/2**3)="+str(sen22beta))

sen222beta = calculaSen2b(sen22beta)
print("Tenemos sen(2*2B)=sen(alpha/2**3)="+str(sen222beta))

sen2222beta = calculaSen2b(sen222beta)
print("Tenemos sen(2*2B)=sen(alpha/2**3)="+str(sen2222beta))

sen22222beta = calculaSen2b(sen2222beta)
print("Tenemos sen(2*2B)=sen(alpha/2**3)="+str(sen22222beta))
