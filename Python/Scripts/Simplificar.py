
def simplificar(polinomio):
    lista = []
    for l in polinomio:
        if [l[0],0] not in lista:
            lista.append([l[0],0])
    for i in polinomio:
        for e in lista:
            if i[0] == e[0]:
                e[1] = e[1] + i[1]
    return(lista)
