#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 11:45:06 2020

@author: dario
"""

def miRaiz(a, e = 0.0001):
    """
    Regresa una aproximación a la raíz cuadrada del número a.
    Se calcula construyendo rectángulos de área a cuyos lados 
    sean cada vez mas parecidos.
    Se considera una áproximación aceptable cuando la diferencia
    de las longitudes de los lados es menor al error de 0.001
    """
    b = 1
    h = a
    while abs(b - h)>e:
        b = (b + h)/2
        h = a/b
    return b

def abs1(x):
    if x == 0:
        return x
    elif x < 0:
        x = -1 * x
        return x
    elif x > 0:
        return x

def abs2(x):
    if x == 0:
        return x
    elif x < 0:
        x = -1 * x
        return x
    else:
        return x
    
def abs3(x):
    resultado = 0
    if x >= 0:
        resultado = x
    else:
        resultado = -x
    return resultado
    
def mcd(a, b):
    """
    Parameters
    ----------
    a : entero
        primer entero para mcd
    b : entero
        segundo entero para mcd
    """
    
    q = a // b
    r = a - b*q
    while r != 0:
        a = b
        b = r
        q = a // b
        r = a - b*q
    return b

#print(mcd(85,25))

#num1 = int(input("Introduce el primer numero: "))
#num2 = int(input("Introduce el segundo numero: "))
def euclides(num1,num2):
    if num2 == 0:
        return num1
    return euclides(num2, num1 % num2)
#print("El máximo común divisor de ", num1," y ", num2," es ", euclides(num1, num2))
 
        
n = 10


# def suma(n):
#     suma = 0
#     for n in range(1, n+1):
#         suma += n
#         n -= 1
# print("La suma de enteros es:", suma)

def sumaPares(n):
    suma = 0
    while n > 0:
        if (n % 2) == 0:
            suma += n
        n -= 1
    print("La suma de los numeros pares es:", suma)
    


def sumaParesFor(n):
    suma = 0
    for n in range(1,n+1):
        if (n % 2) == 0:
            suma += n
        n -= 1
    print("La suma de los numeros paresFor es:", suma)
     

  
def sumaImpares(n):
    suma = 0
    while n > 0:
        if (n % 2) != 0:
            suma += n
        n -= 1
    print("La suma de los numeros impares es:", suma)


def sumaImparesFor(n):
    suma = 0
    for n in range(1,n+1):
        if (n % 2) != 0:
            suma += n
        n -= 1
    print("La suma de los numeros imparesFor es:", suma)



def numeracion(n):
    for i in range(1,n+1):
        print(i)
        


def numeracionDecreciente(n):
    for i in range(n):
        print(n-i)
        

def sigULAM(n): 
    if n % 2 == 0:
        resultado = n/2
    else:
        resultado = 3*n + 1
    return resultado

#n = int(input("Ingrese el valor inicial:\n "))
def ULAM(n):
    while n>1:
        if n % 2 == 0:
            n = n/2
        else:
            n = 3*n + 1
        print(n)
    return n
#ULAM(n)

def mcd2(c, d):
    resto = 0
    while (d > 0):
        resto = d
        d = c % d
        c = resto
    return c


def MCD(a, b, *c):
    resultado = mcd(a,b)
    for valor in c:
        resultado = mcd(resultado,valor)
    return(resultado)


##def mcm(a,b):
##	r = a*b
##	for i in range(a*b):
##		if (((a*b)-i) % a == 0) and (((a*b)-i) % b == 0):
##			r = (a*b)-i
##	return(r)    
