
def contar(cadena):
    num = '1234567890'
    cadena = cadena.lower()
    l = 0
    d = 0
    for i in cadena:
        if i in num:
            l += 1
        elif i in '0123456789':
            d += 1
    return('Son '+str(l)+' letras y '+str(d)+' dígitos.')

# ----------------------------------------------------------------------------------------

def perro_chico(e):
    p = [15,24,28,32,36,40,44,48,52,56,60,64]
    return(p[e-1])

# ----------------------------------------------------------------------------------------

def perro(e,t):
    t = t.lower()
    if t == 'chico':
        p = [15,24,28,32,36,40,44,48,52,56,60,64]
    elif t == 'mediano':
        p = [15,24,28,32,36,42,47,50,51,60,65,69]
    elif t == 'grande':
        p = [15,24,28,32,36,45,50,55,61,66,72,77]
    else:
        return('Los tamaños aceptados son: chico, mediano y grande.')
    return(p[e-1])

# ----------------------------------------------------------------------------------------

def contra(c):
    LETRA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÑ'
    letra = 'abcdefghijklmnopqrstuvwxyzñ'
    car = '$#@'
    num = '0123456789'
    a = 0
    if ' ' in c:
        return("No debe incluir espacios")
    if len(c) < 6:
        return('Deben ser al menos 6 caracteres')
    elif len(c) > 16:
        return('Deben ser menos de 16 caracteres')
    for i in LETRA:
        if i in c:
            a = 1
    if a == 0:
        return('Incluye una mayúscula')
    for i in letra:
        if i in c:
            a = 2
    if a  == 1:
        return('Incluye una minúscula')
    for i in car:
        if i in c:
            a = 3
    if a == 2:
        return('Incluye un caracter de entre $,@ y #')
    for i in num:
        if i in c:
            a = 4
    if a == 3:
        return('Incluye un número')
    return('Listo')
