# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 23:35:42 2020

@author: dario
"""


from utilidades import limpiaPantalla
limpiaPantalla()
#primero manual
i=1.041
interes = i**4 + i**3 + i**2 + i**1
v = 50000-10000*(1.041**4)
x = v/interes

#x=6686.206368384779
a = (10000+x)*1.041
b = (a+x)*1.041
c = (b+x)*1.041
d = (c+x)*1.041
print("Verificación del resultado calculado en forma manual\n")
print("El valor al final del quinto año es: %.2f"
      " abonando a la cuanta %f pesos al final de cada año.\n"%(d,x))

def monto(i, n):
    """
    Calcula el factor de rendimiento para un periodo de n años 
    a una taza de interés del i porciento.
    Ejemplo
    monto(0.041,4)
    """
    resultado = ((1+i)**(n+1))/i - (1+i)/i
    return resultado
i = 0.041
n = 4
x = ( 50000-10000*(1+i)**(n) )/monto(i,n)
print("El valor de x calculado utilizando la función monto es %.2f"%(x))

def problema4_2(montoInicial,montoFinal,i,n):
    """
    Calcula la cantidad anual x que se debe abonar a una cuenta de ahorro
    que tiene montoInicial de pesos al unicio del primer año, paga el i 
    porciento de interés anua con la finalidad de obtener despues de n
    años montoFinal de pesos
    """
    resultado = x
    print("La cantidad que se debe depositar al principo de cada año para ahorrar"
          " un total de %f pesos, durante %d años,"
          "es: %.2f. La tasa de interés utilizada para el cálculo es del %.3f por ciento "
          "durante %d años.\nEl monto inicial es de %.2f\n"%(montoFinal, n, resultado, i, n, montoInicial))
    
problema4_2(10000,50000,4.1,5)

x = ( 50000-100*(1+i)**(n) )/monto(i,n)
problema4_2(100,50000,6.0,10)

x = ( 500000-100*(1+i)**(n) )/monto(i,n)
problema4_2(100,500000,6.0,35)


from miMat import miRaiz

z = 9
print("La raiz cuadrada de {0:.2f} es {1}".format(z, miRaiz(z)))
