# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 23:35:42 2020

@author: dario
"""

from sympy import symbols, solve
x, a, b, c, d = symbols("x a b c d")

a = (10000+x)*1.041
b = (a+x)*1.041
c = (b+x)*1.041
d = (c+x)*1.041
e = 50000-d
print(solve(e))