#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 12:06:01 2020

@author: dario
"""

from math import sqrt

a = int(input("Ingrese lado a:\n"))
b = int(input("Ingrese lado b:\n"))
c = int(input("Ingrese lado c:\n"))
p = (a+b+c)
s = (a+b+c)/2
Area=(sqrt(s*(s-a)*(s-b)*(s-c)))
def triangulo(a, b,c):
    if((abs(a-c)<b)and(b<(a+c))):
        print("\nSi Forma un triangulo")
        print('Su perimetro es de {0}'.format(p))
        print('Su area es de {0}'.format(Area))
        if(a==b and b==c):
            print("\nEl triangulo es equilatero")
        elif(a==b or a==c or b==c):
            print("\nEl triangulo es isoceles")
        elif (a!=b or a!=c or b!=c):
            print("\nEl triangulo es escaleno")
    else:
        print("No Forma un triangulo")
triangulo(a,b,c)


from math import sqrt

a = int(input("Ingrese lado a:\n"))
b = int(input("Ingrese lado b:\n"))
c = int(input("Ingrese lado c:\n"))
def rectangulo(a, b, c):
    if ((a,b != 0) and (c == 0)):
        print('\nLa hipotenusa es {0}'.format(sqrt(a**2 + b**2)))
    elif ((a,c != 0) and (b == 0)):
        print('\nEl cateto adyacente es {0}'.format(sqrt(c**2 - a**2)))
    elif ((b,c != 0) and (a == 0)):
        print('\nEl cateto opuesto es de {0}'.format(sqrt(c**2 - b**2)))
rectangulo(a, b, c)


a = int(input("Ingrese angulo a:\n"))
b = int(input("Ingrese angulo b:\n"))
c = int(input("Ingrese angulo c:\n"))
def clasificacionAngulos(a, b, c):
    if ((a == 90) or (b == 90) or (c == 90)):
        print('El angulo es recto')
    elif ((a > 90) or (b > 90) or (c > 90)):
        print('El angulo es obtuso')
    elif ((a < 90) or (b < 90) or (c < 90)):
        print('El angulo es agudo')
clasificacionAngulos(a, b, c)


from math import sin, acos, sqrt, cos, asin

def Triangulo(a,b,c,A,B,C):
    if ( a != 0 and b == 0 and c == 0 and A == 0 and B != 0 and C != 0 ):
        A = 180 - B - C
        b = (a * sin(B))/sin(A)
        c = (a * sin(C))/sin(A)
        print('Se conoce un lado y sus dos angulos adyacentes. Mi angulo A es {0}, y mis lados b y c son {1} y {2}'.format(A, b, c))
    elif ( a != 0 and b == 0 and c == 0 and A != 0 and B == 0 and C != 0 ):
        B = 180 - A - C
        b = (a * sin(B))/sin(A)
        c = (a * sin(C))/sin(A)
        print('Se conoce un lado, uno de sus angulos adyacentes y otro angulo, el opuesto. Mi angulo B es {0}, y mis lados b y c son {1} y {2}'.format(B, b, c))
    elif ( a != 0 and b != 0 and c == 0 and A == 0 and B == 0 and C != 0 ):
        c = sqrt( (a**2 + b**2) - ((2*a*b) * cos(C) ))
        A = acos((a * sin(C))/c)
        B = 180 - A - C
        print('Se conocen dos lados y el angulo que forman estos. Mi angulo A es {0}, mi angulo B es {1} y mi lado c es {2}'.format(A, B, c))
    elif ( a == 0 and b != 0 and c != 0 and A == 0 and B == 0 and C != 0 ):
        B = asin((b * sin(C))/c)
        A = 180 - B - C
        a = (b * sin(A))/sin(B)
        print('Se conocen dos lados y un angulo diferente al que forman estos. Mi angulo A es {0}, mi angulo B es {1} y mi lado a es {2}'.format(A, B, a))
    elif ( a != 0 and b != 0 and c != 0 and A == 0 and B == 0 and C == 0 ):
        A = acos(( b**2 + c**2 - a**2)/2*b*c)
        B = acos(( a**2 + c**2 - b**2)/2*a*c)
        C = 180 - A - B
        print('Se conocen los tres lados. Mi angulo A es {0}, mi angulo B es {1} y mi angulo C es {2}'.format(A, B, C))

Triangulo(3,9,0,0,0,48)

