# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 13:44:08 2020

@author: dario
"""


cadena = "The quick red fox jumps over the lazy red dog"

def contarVocales(cadena):
    n = 0
    
    for c in cadena:
        if c in "aeiouAEIOUáéíóúÁÉÍÓÚü":
            n+=1
    print("Hay %i vocales en: \"%s\"." %(n,cadena))
    
contarVocales(cadena)


def contarConsonantes(cadena):
    n = 0
    
    for c in cadena:
        if c in "qwrtypñlkjhgfdszxcvbnmQWRTYPÑLKJHGFDSZXCVBNM":
            n+=1
    print("Hay %i consonantes en: \"%s\"." %(n,cadena))
    
contarConsonantes(cadena)


def contarMayusculas(cadena):
    n = 0
    
    for c in cadena:
        if c in "QWERTYUIOPASDFGHJKLÑZXCVBNM":
            n+=1
    print("Hay %i mayúsculas en: \"%s\"." %(n,cadena))

contarMayusculas(cadena)


def contarMinusculas(cadena):
    n = 0
    
    for c in cadena:
        if c in "qwertyuiopasdfghjklñzxcvbnm":
            n+=1
    print("Hay %i consonantes en: \"%s\"." %(n,cadena))

contarMinusculas(cadena)

def palindromo(c):
    c = c.lower()
    c = c.replace(" ","")
    ca = ""
    for i in c:
        ca = i + ca
    if ca == c:
        print("Sí")
    else:
        print("No")

palindromo(cadena)
palindromo("yaAAAAaaa aa aa   aY")
    
    

    
