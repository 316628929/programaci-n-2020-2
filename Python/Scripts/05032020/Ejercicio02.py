#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 21:36:20 2020

@author: dario
"""

p = float(input("Escribe la puntuación:\n "))
def Puntuacion(p):
    if p == 0.0:
        print( f"Calificación inaceptable, fue de {p}.\n Ingreso: ${p*2400}" )
    elif p == 0.4:
        print( f"Calificación aceptable, fue de {p}.\n Ingreso: ${p*2400}" )
    elif p == 0.6:
            print( f"Calificación meritorio, fue de {p}.\n Ingreso: ${p*2400}" )
    else:
        raise SyntaxError( "Ponga un valor válido ( 0.0, 0.4, 0.6 ) ")
Puntuacion(p)
