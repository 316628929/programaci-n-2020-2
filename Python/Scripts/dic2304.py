## 1) Implementa un programa que almacene un diccionario
## con los créditos de las materias de un semestre la key
## tiene que ser el nombre de la materia y el valor asignado
## el número de créditos, después se debe mostrar el siguiente
## mensaje "(Nombre de la materia) tiene (Número de créditos) ".
## Hint: Puede servir el método dict.items() (busquénlo en google)

d = {'Programación':'10','Álgebra':'10','Geometría':'10','Cálculo':'18','Ciencia básica':'10'}

for i in d:
    print("%s tiene %s créditos" %(i, d[i]))
    
print()

## 2) Cuenta el número de ocurrencias de palabras en el siguiente texto dado:
## (Deben Utilizar un diccionario para almacenar las palabras y el número de
## veces que aparecieron)
## "Este es un texto de prueba, queremos utilizar este texto para hacer una
## prueba de nuestro programa

Texto = 'Este es un texto de prueba, queremos utilizar este texto para hacer una prueba de nuestro programa'

for i in Texto:
    if i not in 'QWERTYUIOPASDFGHJKLÑZXCVBNM qwertyuiopasdfghjklñzxcvbnm ÁÉÚÍÓ áéúíó':
        Texto.replace(i,"")

texto = Texto.split()

di = {}

for i in texto:
    di[i] = 0

for i in texto:
    if i in di:
        di[i] += 1

print(di)
print()


## 3) Muestra los productos con el precio menor o igual al que introduce el usuario.
## (Debes utilizar un diccionario para almacenar los productos)
##
## Productos:
##
## Computadora $500
## Televisión $300
## Radio $400
## Teclado $500
## Mouse $100
## Ventilador $200

dic = {'Computadora':500,'Televisión':300,'Radio':400,'Teclado':500,'Mouse':100,'Ventilador':200}

p = float(input('Introduce el precio'))
l = []
c = 0

for i in dic:
    if dic[i] <= p:
        l.append(i)
        c += 1

if c == 0:
    print("Te alcanza para nada")
   
print(l)
