
from re import findall, match, search

# match - matches to the first, returns ndex
# search - True or False

# ? - implica una aparición o ninguna, a su lado izquierdo


def empatatexto(texto):
    patron = 'ab*'
    if search(patron,texto):
        return('Sí')
    else:
        return('No')

print(empatatexto('ac'))
print(empatatexto('abc'))
print(empatatexto('abbc'))

print()

# ------------------------------------------------

def empatatexto(texto):
    patron = 'ab+'
    if search(patron,texto):
        return('Sí')
    else:
        return('No')

print(empatatexto('ac'))
print(empatatexto('abc'))
print(empatatexto('abbc'))

print()

# ------------------------------------------------

def empatatexto(texto):
    patron = 'ab{3}'
    if search(patron,texto):
        return('Sí')
    else:
        return('No')

print(empatatexto('ac'))
print(empatatexto('abc'))
print(empatatexto('abbc'))
print(empatatexto('abbbc'))
print(empatatexto('abbbbc'))

print()

# ------------------------------------------------

def carpermitido(string):
    string = search('[a-zA-Z0-9]',string)
    return bool(string)

print(carpermitido('ABCáDEDDreagg44f'))
print(carpermitido('!"$#&/%%%'))

print()

# ------------------------------------------------

def guionbajo(string):
    if search('[a-ñ]_[a-ñ]',string):
        return('Sí')
    else:
        return('No')

print(guionbajo('qwery_quy'))
print(guionbajo('qweryquy'))

print()

# ------------------------------------------------
    
def guionbajo(string):
    return(bool(search('[a-ñ]_[a-ñ]',string)))

print(guionbajo('qwery_quy'))
print(guionbajo('qweryquy'))
    
