##cadena1 = input('Escribe la primera cadena')
##cadena2 = input('Escribe la segunda cadena')

cadena1 = 'pé drÖ'
cadena2 = 'POD.??ER'

def normalizar(cadena):
    cadena = cadena.lower()
    
    caracteres = 'qwertyuiopasdfghjklñzxcvbnmáéúíóäëïöüâêîôûàèìòù'
    acentos = ['á','é','í','ó','ú','ä','ë','ï','ö','ü','â','ê','î','ô','û','à','è','ì','ò','ù']
    noacento = ['a','e','i','o','u','a','e','i','o','u','a','e','i','o','u','a','e','i','o','u']

    for i in cadena:
        if i not in caracteres:
            cadena = cadena.replace(i,"")

    for i in acentos:
        cadena = cadena.replace(i,noacento[acentos.index(i)])

    return(cadena)

cadena1 = normalizar(cadena1)
cadena2 = normalizar(cadena2)

if sorted(cadena1) == sorted(cadena2):
    print('Sí es anagrama')
else:
    print('No es anagrama')



#----------------------------------------------------------------------


##cadena1 = 'Peedró'
##cadena2 = 'pod ,,é,E r'

d1 = {}
d2 = {}

cadena1 = cadena1.lower()
cadena2 = cadena2.lower()

for i in cadena1:
    if i not in 'QWERTYUIOPASDFGHJKLÑZXCVBNMqwertyuiopasdfghjklñzxcvbnmÁÉÚÍÓáéúíó':
        cadena1 = cadena1.replace(i,"")

for i in cadena2:
    if i not in 'QWERTYUIOPASDFGHJKLÑZXCVBNMqwertyuiopasdfghjklñzxcvbnmÁÉÚÍÓáéúíó':
        cadena2 = cadena2.replace(i,"")

acentos = ['á','é','í','ó','ú']
noacento = ['a','e','i','o','u']

for i in acentos:
    cadena1 = cadena1.replace(i,noacento[acentos.index(i)])
    cadena2 = cadena2.replace(i,noacento[acentos.index(i)])

for i in cadena1:
    d1[i] = 0

for i in cadena1:
    d1[i] +=1

for i in cadena2:
    d2[i] = 0

for i in cadena2:
    d2[i] +=1

if d1 == d2:
    print('Sí es anagrama')
else:
    print('No es anagrama')
