# Función de triángulo de Pascal hecha con seis líneas.

def pascal(n):
    listan = [1]
    for x in range(0,n+1):
        print(listan)
        lista=listan.copy()
        listan = [1]+[lista[i-1]+lista[i] for i in range(1,x+1)]+[1]
