
"""
a es el valor original al 
que necesito calcular el
seno

b es el valor del ultimo
seno
"""
from math import sqrt, sin

a = 0.1
b = a/(2**5)

b2 = 2*b*sqrt(1-b**2)
b4 = 2*b2*sqrt(1-b2**2)
b8 = 2*b4*sqrt(1-b4**2)
b16 = 2*b8*sqrt(1-b8**2)
b32 = 2*b16*sqrt(1-b16**2)

b = 2*b*sqrt(1-b**2)
b = 2*b*sqrt(1-b**2)
b = 2*b*sqrt(1-b**2)
b = 2*b*sqrt(1-b**2)
b = 2*b*sqrt(1-b**2)

def Seno(a,n=5):
    """
    Regresa una aprocimación del seno de a 
    haciendo cinco iteraciónes
    """
    b = a/(2**n)
    for i in range(n):
        b = 2*b*sqrt(1-b**2)
    return b

print(Seno(5))
print(Seno(5,10))
print(Seno(5,15))
print(sin(5))


"""
print(b32)
print(b)
print(Seno(a))
print(sin(a))
"""
