def Kaprekar(numero):
    #numero = input()
    numero=str(numero)
    veces = 0

    print("Para llegar al número de Kaprekar desde %s se hacen las siguientes operaciones:" % (numero))

    while int(numero) != 6174 :
        lista = []

        for i in list(numero):
            a = int(i)
            lista.append(a)
            
        lista.sort()
        LISTA = lista.copy()
        LISTA.reverse()

        lista = int(''.join(list(map(str,lista))))
        LISTA = int(''.join(list(map(str,LISTA))))

        resultado = LISTA - lista

        print("%i - %i = %i" %(LISTA,lista,resultado))
        
        veces += 1
        
        numero = str(resultado)

    print("Se tiene que hacer el proceso %i veces" % (veces))
